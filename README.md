![CABS-flex-logo-1401.jpg](https://bitbucket.org/repo/KrXpGzE/images/2430462593-CABS-flex-logo-1401.jpg)

### This is a repository for CABS-flex standalone application developed by the [Laboratory of Computational Biology](http://lcbio.pl). Detailed instructions and tutorials are provided on [CABS-flex WIKI PAGE](https://bitbucket.org/lcbio/cabsflex/wiki). Any issues should be reported using [CABS-flex Issue Tracker](https://bitbucket.org/lcbio/cabsflex/issues)
---
# ABOUT THE METHOD

CABS-flex method has been first made available as the CABS-flex web server and recently updated to [CABS-flex web server 2.0](http://biocomp.chem.uw.edu.pl/CABSflex2). The standalone application version [[Bioinformatics 35:694–695, 2019]](https://doi.org/10.1093/bioinformatics/bty685) provides the same modeling methodology equipped with many
additional features and customizable options. The publications describing the CABS-flex method/ web server/ and its example applications have been listed at the laboratory website: http://lcbio.pl/tools/cabsflex

---
### Table of contents
[1. Introduction](#markdown-header-introduction)

[2. Windows installation guide](#markdown-header-windows-users)

[3. Linux installation guide](#markdown-header-linux-users)  
  
[4. macOS installation guide](#markdown-header-macos-users)

[5. Docker image](#markdown-header-docker-image)

---

## Introduction
*CABS-flex* can be installed on Linux, macOS or Windows. This comprehensive tutorial is aimed to guide users through the installation process and is split into three parts for corresponding systems. 

*CABS-flex* is a python package using *Python 2.7* version. We *highly* recommend that you use the scientific Anaconda distribution that comes with pre-installed packages 
and conda package manager which allows for an easier and less error prone installation. If you choose to use Anaconda simply go to [their web page](https://www.anaconda.com/download/)
and download a Python 2.7 installation for your target operating system. Further steps will give specific instructions for conda users as well as for vanilla python.

##### Tips for users who are not familiar with Python or using command prompt/ terminal:
- *Python3.X* is **NOT** the latest version of *Python2.7* and you should always use *Python2.7* to run CABS-flex.
- Throughout this tutorial a PATH will be frequently mentioned. PATH is a variable that tells the system where to look for your installed programs when you issue commands. Detailed instructions on how to modify it will be given in appropriate parts of this guide.
- Most sections of this guide contain a correctness test step which can be executed to see wether or not the isntallation was successful. When in doubt run the test. 
- While this tutorial attempts to covers much ground as possible if something goes wrong please contact us for assistance this will also help us improve our software and this guide too!

---

## Windows users

#### 1. GFortran 

GFortran is mandatory for *CABS-flex* program. We recommend that you use the [MinGW-w64 (download page)](https://sourceforge.net/projects/mingw-w64/?source=typ_redirect) or [MinGW](http://www.mingw.org/)   
Both provide graphical installers and default settings should be fine for most users (be sure to select gfortran for MinGW when selecting packages).

Once you have installed one of the distributions GFortran is present on your system but not yet ready to use in command prompt as it's not on your PATH variable (Windows doesnt know what gfortran command is).
There are two ways to fix that (and this will also be the case for Python and pip later on). You can either add the folder containing the gfortran.exe 
(for example C:\MinGW\bin or C:\mingw32\bin) to your path for all command prompt sessions:

- Go to Control Panel->All Control Panel Items->System find the Advanced System Settings and then click on the Environment Variables button
- Select Path variable in the system variables table and click edit
- Add the folder as a new entry, confirm the changes and leave the screen. 
- Programs present in the added folder will now be available as commands in all new command prompt sessions (you need to restart the prompt if you had it open)

To add something to a Windows PATH just for one command prompt session issue a following command with your respective path:

```
set PATH=%PATH%;C:\path\to\gfortran
```

* Note that You have to repeat this step each time you want to use gfortran (or run *CABS-flex*) 
##### Correctness test 
To verify that everything went well simply type ```gfortran``` in your command prompt. If the output is ```gfortran: fatal error: no input files``` that means gfortran is available.
If the output is ```'gfortran' is not recognized as an internal or external command, operable program or batch file. ``` It means that gfortran is not on your PATH and it needs to be added. 

#### 2. Python 2.7 

##### Anaconda distribution 

If you have chosen to use Anaconda this step is already done. 

* Please do note that at this point you should always use  the Anaconda Prompt available in the start menu under that name instead of the regular command prompt.

##### Vanilla Python distribution   

- Follow instructions on [python.org](https://python.org). During the installation process select to include pip and setuptools as those will be necessary. 
- Add Python27/Scripts (by default under C:\Python27\Scripts) folder to your PATH. This folder contains pip which will install CABS as well as it is the folder which will contain the CABS-flex program once its installed.

Follow the steps described in GFortran section. The folder should be located at ```C:\Python27\Scripts``` by default so either follow the steps in the Control Panel or type:

```
set PATH=%PATH%;C:\Python27\Scripts
``` 

- Note that you have to re-type that in each command prompt in which you want to use *pip* or *CABS-flex*.  

##### pip not installed with Python

If you chose not to install pip with your Python installation you need to do that manually. 
Assuming you've already made Python2.7 accessible under command "python", download [this](https://bootstrap.pypa.io/get-pip.py) script, 
change to the directory with downloaded script and run:

```python get-pip.py```
 
##### Correctness test 
To check if pip (hence also Python) are working open a  `command
prompt` (press `cmd + R`; enter "cmd"; hit `enter`) and run the following command:

```pip freeze```

This should list all the packages that you have currently installed.

If you wish to check that you have the correct Python version add the Python binary (python.exe) replacing the example path with your respective one:

```set PATH=%PATH%;C:\Python27``` 

And then type:

```python --version```

Minimum recommended python version for CABSflex is 2.7.12 but as low as 2.7.6 should work. We recommend that you download the latest version from
[python.org](https://python.org).

#### 3. Modeller (optional)

Modeller is required for reconstructing all-atom models from the generated CABS models. See the wiki for more details.
To install Modeller go to [program's web page](https://salilab.org/modeller/download_installation.html) and follow the instructions there.
- Use the conda installer for Anaconda and regular installer for vanilla Python 

##### Correctness test 
Run python in your desired environment (for example type `python` in your command prompt) and issue the following command:

```import modeller```

If no ```ImportError``` occurs it means you have successfully installed modeller.  
#### 4. *CABS-flex*
##### Anaconda users
In your Anaconda Prompt type:

```conda install -c lcbio cabs```

##### Vanilla Python
In the regular command prompt type:

```pip install cabs```

##### Correctness test 
Run a simulation of lcbio's favorite 2gb1 with:

```CABSflex -i 2gb1 -w test_run -v 4```

- If the result is `'CABSflex' is not recognized as an internal or external command, operable program or batch file.` it means that your Python's Scripts folder is not on the PATH variable.
- If the simulation log shows ` Missing FORTRAN compiler (gfortran).` it means gfortran is not installed or not on the PATH
- If you see a ```Simulation completed successfully``` message, congratulations you have completed your first *CABS-flex* simulation. 

---

## Linux users ##

##### Please note that because of how many linux distributions  are out there:
- We recommend Anaconda Python distribution for best package availability and compatibility.
- We can't test each of the distributions and some older ones might lack necessary packages, never versions of libraries, etc. 
- We can't provide specific instructions for each of the distributions. We will consider ubuntu in this tutorial but converting the instructions to other distributions should be pretty straight forward.

#### 1. GFortran 
GFortran should be installed by default on most unix systems. If it is not available install it via the systems package manager:

```sudo apt-get install gfortran```

Conda users can also install GFortran via conda:

```conda install -c anaconda gfortran_linux-64```

- For unix we **strongly** advise to use the system's package manager as conda will install your GFortran under a different name. If GFortran is somehow not available otherwise 
the simplest way to get this variant working is to copy the GFortran conda binary (found in your ```/path/to/anaconda2/bin/``` with a name like x86_64_conda_cos6-linux-gnu-gfortran) to a file called simply gfortran in that same directory
Example command:

```cp ~/anaconda2/bin/x86_64_conda_cos6-linux-gnu-gfortran ~/anaconda2/bin/gfortran```

This is however a hack and should be avoided as it might cause problems if you ever intend to use a different GFortran version etc. 

- For the conda GFortran version to work anaconda2/bin folder still needs to be on your PATH
##### Correctness test 
To verify that everything went well simply type ```gfortran``` in your terminal. If the output is ```gfortran: fatal error: no input files``` that means GFortran is available.
If the output is ```No command 'gfortran' found. ``` It means that GFortran is not installed. 

#### 2. Python 2.7 

##### Anaconda distribution 

If you have chosen to use Anaconda this step is already done. Anaconda installer should ask you if you want it to be added to your PATH, 
for most users this is desirable because it means that when typing ```python``` it will call Anaconda's Python rather than regular one.  

* Check if the ```python``` command refers to Anaconda Python type:

```python --version```

The output should be something like ```Python 2.7.14 :: Anaconda, Inc. ```. If it is not add a following line to your ~/.bashrc file (found in the user's home directory) 
replacing the path with your anaconda's installation path:

```export $PATH="/absolute/path/to/anaconda2/bin:$PATH"```

and then close and reopen the terminal or simply run:

```source ~/.bashrc```

##### Vanilla Python distribution   

Python 2.7 should be present on all unix systems, to verify the version, open your terminal and type:

```python --version```

Minimum recommended python version for CABSflex is 2.7.12. (but as low as 2.7.6 should work too. Lower versions were not tested)

##### pip not installed with Python

pip should also be installed by default on most unix systems, verify that it works issuing the following command:

```pip freeze```

This should return a list of installed packages for your Python. 
if that is not the case install it using your system's package manager:

```sudo apt-get install python-pip```
 
#### 3. Modeller (optional)

Modeller is required for reconstructing all-atom models from the generated CABS models. See the wiki for more details.
To install Modeller go to [program's web page](https://salilab.org/modeller/download_installation.html) and follow the instructions there.
- Use the conda installer for Anaconda and regular installer for vanilla Python 

##### Correctness test 
Run python in your desired environment and issue the following command:

```import modeller```

If no ```ImportError``` occurs it means you have successfully installed modeller.  
#### 4. *CABS-flex*
##### Anaconda users
Simply type:

```conda install -c lcbio cabs```

##### Vanilla Python
Simply type:

```pip install cabs```

##### **Troubleshooting**

- If pip install fails due to writing rights **do not use sudo pip install.** 

- Depending on your privileges and the way your system is managed you might want or need to install *CABS-flex* just for the current user:

```pip install --user cabs```

- When using the --user flag the binary is placed in a folder that is usually not on your PATH. Usually it is located in your 
```$HOME/.local/bin``` but check if that is the case before continuing. Once you located the binary add its location to your path via editing your .bashrc folder in home directory. Add the line:

```export PATH="$HOME/.local/bin:$PATH"```

Then reopen the terminal or do a ```source ~/.bashrc```. 

- When using the --user flag with pip isntall the command which will be available after appending the correct folder to PATH is ```cabsflex```  rather than ```CABSflex```. This is due to the fact that Python pacakge mangares enforce a lowercase naming covention for packages. 

- Some additional packages which are prerequisites to Python libraries might not be present on the system, list of known-of candidates can be fixed via:

```sudo apt-get install pkg-config libfreetype6-dev libpng12-dev python-dev python-tk```





#### Correctness test 
Run a simulation of lcbio's favorite 2gb1 with:

```CABSflex -i 2gb1 -w test_run -v 4```

- If the result is `CABSflex: Command not found` it means that your *CABS-flex* installation failed or you installed into a folder that is not recognised by your system (not on your PATH)
- If the simulation log shows ` Missing FORTRAN compiler (gfortran).` it means gfortran is not installed 
- If you see a ```Simulation completed successfully``` message, congratulations you have completed your first *CABS-flex* simualtion. 

---

## macOS users


#### 1. GFortran 

- Instructions for how to install GFortran can be found [here](https://gcc.gnu.org/wiki/GFortranBinariesMacOS).

Conda users can also install GFortran via conda:

```conda install -c anaconda gfortran_osx-64```

- While we strongly advise against this approach in unix systems this might be an easier path for some macOS users. 

The Anaconda GFotran conda binary has a different name than just gfortran so your system won't recognize it at first.
The easiest trick to make it work is to copy the binary (found in your ```/path/to/anaconda2/bin/``` which is a long-named file ending with gfotran) to a file called simply gfortran in that same directory
Example command (the filename might vary):

```cp ~/anaconda2/bin/x86_64_conda_cos6-osx64-gnu-gfortran ~/anaconda2/bin/gfortran```

This is however a hack and should be avoided as it might cause problems if you ever intend to use a different GFortran version etc. 

- For the conda GFortran version to work anaconda2/bin folder still needs to be on your PATH


##### Correctness test 
To verify that everything went well simply type ```gfortran``` in your terminal. If the output is ```gfortran: fatal error: no input files``` that means GFortran is available.
If the output is ```No command 'gfortran' found. ``` It means that GFortran is not installed. 

#### 2. Python 2.7 

##### Anaconda distribution 
If you have chosen to use Anaconda this step is already done. Anaconda installer should ask you if you want it to be added to your PATH, 
for most users this is desirable because it means that when typing ```python``` it will call Anaconda's Python rather than regular one.  
* Check if the ```python``` command refers to Anaconda Python type:

```python --version```

If the result doesnt include the word Anaconda it means your system is using other Python version by default this can be changed by prepending Anaconda to your PATH.

- Move into home directory
- Create a .bash_profile file using a text editor (like nano) ```nano .bash_profile```
- Add the line ```export PATH="/absolute/path/to/anaconda2/bin:$PATH"``` with your respective path to anaconda2 installation
- Save the file and relaunch the terminal 

##### Vanilla Python distribution   

macOS comes with *Python2.7* already installed. To check if you have the correct Python version open the `Terminal.app` and type:

```python --version```

If you get the message: `bash: python: command not found` it may mean that your system doesn't have Python installed, or
Python's binary is not in the system `PATH`. To check this run in the `Terminal.app` the following command:

```/Library/Frameworks/Python.framework/Versions/2.7/bin/python --version```

If you still get the message: `bash: python: command not found` you need to install *Python2.7*. Otherwise add Python's
binary to the system's `PATH` by running in the `Terminal.app` the following command and tehn reopen the terminal:

```echo "export PATH=/Library/Frameworks/Python.framework/Versions/2.7/bin/:$PATH" >> ~/.bash_profile```

##### pip not installed with Python

Assuming you've already installed Python2.7 and made it accessible under command "python" simply install pip via setuptools by:

```sudo easy_install pip```

#### 3. Modeller (optional)

Modeller is required for reconstructing all-atom models from the generated CABS models. See the wiki for more details.
To install Modeller go to [program's web page](https://salilab.org/modeller/download_installation.html) and follow the instructions there.
- Use the conda installer for Anaconda and regular installer for vanilla Python 

##### Correctness test 
Run python in your desired environment and issue the following command:

```import modeller```

If no ```ImportError``` occurs it means you have successfully installed modeller.  
#### 4. *CABS-flex*
##### Anaconda users
Simply type:

```conda install -c lcbio cabs```

##### Vanilla Python
Simply type:

```pip install cabs```

#### Correctness test 
Run a simulation of lcbio's favorite 2gb1 with:

```CABSflex -i 2gb1 -w test_run -v 4```

- If the result is `CABSflex: Command not found` it means that your *CABS-flex* installation failed or you installed into a folder that is not recognised by your system (not on your PATH)
- If the simulation log shows ` Missing FORTRAN compiler (gfortran).` it means gfortran is not installed 
- If you see a ```Simulation completed successfully``` message, congratulations you have completed your first *CABS-flex* simualtion. 

## Docker image

*CABS-flex* is also available as a docker image for users facing compatibility issues or those who intend to scale the app 
 and would like to do it using the container technology. 
 
*Please note we cannot include Modeller software in distributed images as it requires a licence to run, hence the basic Docker images do not offer all atom reconstruction for results.*
 
We provide two docker images serving different purposes (click on the links for detailed descriptions):

- ```lcbio/cabsflex``` - [conda](https://hub.docker.com/r/lcbio/cabsflex/) based distribution. Included Dockerfile should allow the users to build a Docker Image running Modeller software once a licence key is supplied.
- ```lcbio/cabsflex_min``` - [Debian](https://hub.docker.com/r/lcbio/cabsflex_min/) based distribution. More lightweight but less flexible. 

 
### Beginner's tutorial
 
 While we recommend Docker images only for users already familiar with the technology and cannot provide a ground-up guide 
 on Docker usage we offer some tips and further reading material:
 
#### 1. Installing Docker
 
 Please refer to the [docs](https://docs.docker.com/install/) which contain detailed and well written guides for different systems:
 
 We also recommend you read [this](https://docs.docker.com/get-started/) starter article on the docker docs
 
 And [this](https://docs.docker.com/install/linux/linux-postinstall/) post-installation tips (especially if you get permission errors and have to always run docker with sudo)
 
#### 2. Get the *CABS-flex* images
  
 To make the images available on your system run:

- ```docker pull lcbio/cabsflex```
- ```docker pull lcbio/cabsflex_min```

To test if they work correctly try (replacing cabsflex with your chosen image):

- ```docker run cabsflex CABSflex -h``` 

#### 3. Running the Docker container
 
 *Note that the data produced in your container will be erased by default after it has been deleted. To have data available on the local filesystem one can use the -v flag
 which effectively mounts a folder from your host system inside the container and the data which will be written there will persist even after the container is removed.*
 
 - The syntax goes as follows: ```docker run -v /path/to/your/system/folder:/path/on/your/container```
 - One can also copy the files directly from the container using ```docker cp container_id:path/inside/the/container local/path```
 
 To run *CABS-flex* via docker you can do (explained in detail below):
 
 ```docker run -v results_folder:/home cabsflex CABSflex -i 2gb1 -v 4 -w /home```
 
 * results_folder must exist before running this command
 * cabsflex is the name of your chosen image
 * CABSflex -i 2gb1 -v 4 -w /home will run a CABSflex simulation in the container's /home folder
 
 To interact with the container's console:
 
 ```docker run results_folder:/home cabsflex /bin/bash```
 
 
#### 4. Using Modeller in the container
 
 To do that one has to create their own image based on our conda image.
 
 - Create a new folder and cd into it 
 - Create a Dockerfile and copy the contents which can be found [here](https://hub.docker.com/r/lcbio/cabsflex/)
 - Uncomment the suggested lines and replace XXXX with your Modeller licence key
 - Run ```docker build -t image_name .``` 
 - If the process is successful the image will be available under 'image_name'
 
#### 5. Further reading 
 
 - Running docker images [explained](https://www.pluralsight.com/guides/docker-a-beginner-guide)
 - We found [this](https://docker-curriculum.com/) guide to be quite informative and well-written. 
 
### CABS-flex uses some external software packages for the following tasks:

* [gfortran](https://gcc.gnu.org/wiki/GFortran) - Fortran compiler for the CABS core simulations subroutines.
* [MODELLER](https://salilab.org/modeller/) - program for modeling of protein structure using spatial restraints.
CABSflex uses MODELLER for the reconstruction of predicted complexes from tha C-alpha to all-atom representation.
* [libpng](http://www.libpng.org) - library for the png bitmaps.
* [libfreetype](https://www.freetype.org/) - library for the freetype fonts.

---

------------------------------------------
[Laboratory of Computational Biology](http://lcbio.pl), 2019
